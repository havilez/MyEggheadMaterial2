import { NgModule } from '@angular/core';
import {
  MatListModule,
  MatToolbarModule,
  MatSidenavModule,
  MatIconModule,
  MatButtonModule
} from '@angular/material';


// module which will exports material components used in the app
@NgModule({
  exports: [
    MatListModule,
    MatToolbarModule,
    MatSidenavModule,
    MatIconModule,
    MatButtonModule
  ]
})

export class AppMaterialModule {

};
